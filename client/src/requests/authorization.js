import { BASE_URL } from "../common/constants";

export const signIn = (email, password) =>
  fetch(`${BASE_URL}/auth/login`, {
    method: "POST",
    headers: {
      "Content-Type": "Application/json",
    },
    body: JSON.stringify({
      email,
      password,
    }),
  }).then((r) => r.json());
  
