import React from 'react';
import aboutWallpaper from '../../wallpapers/allcourses-wallpaper2.jpg'

const About = () => {
    return (
      <div
        style={{
      backgroundImage: `url(${aboutWallpaper})`,
      position: "fixed",
      width: "100%",
      height: "100vh",
      backgroundPosition: "center",
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
      top: "0",
      zIndex: "-1",
      overflowY: "scroll",
    }}
      >
        <div style={{
          fontSize: '30px', 
          textAlign: 'center',
          display: 'flex',
          alignItems: 'center',
          marginTop: '17%',
          position: 'absolute',
          right: '550px',
          fontFamily: 'Kaisei HarunoUmi, serif',
          fontStyle: 'oblique',
          color: 'white',
          textShadow: '1px 1px 5px black',
          fontWeight: '650'
        }}
        >
          This site was created by:
          <br />
          Dragomir Krastev
          <br />
          and
          <br />
          Hristo Peynov
        </div>
      </div>
);
};

export default About;

