import React, { useState, useEffect, useContext } from "react";
import { updateUser } from "../../../requests/users";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router";
import './EditUser.css';
import AuthContext from "../../../providers/AuthContext";
import registerValidations from "../../../validators/register-validations";
import Background from '../../../wallpapers/allcourses-wallpaper2.jpg'

const EditUser = (props) => {

    const id = props.match.params.id;
    const history = useHistory();

    const { user } = useContext(AuthContext);

    const [error, setError] = useState(null);
    const [isFormValid, setIsFormValid] = useState(false);
    const [userUpdate, setUserUpdate] = useState({
      firstName: {
        value: props.location.state.userDetails.firstName,
        type: 'text',
        placeholder: 'First name',
        valid: true,
        touched: false,
        fieldReq: "Between 3 and 20 symbols, including letters and digits",
     },
     lastName: {
        value: props.location.state.userDetails.lastName,
        type: 'text',
        placeholder: 'Last name',
        valid: true,
        touched: false,
        fieldReq: "Between 3 and 20 symbols, including letters and digits",
     },
     password: {
        value: '',
        type: 'password',
        placeholder: 'Password',
        valid: true,
        touched: false,
        fieldReq: "Between 4 and 10 symbols, must include both letters and digits",
     },
    });

    const handleInputChange = (e) => {
      const {name, value} = e.target;

      const updatedInput = userUpdate[name];
      updatedInput.value = value;
      updatedInput.valid = registerValidations[name](value);
      updatedInput.touched = true;

      setUserUpdate({...userUpdate, [name]: updatedInput});

      const formValid = Object.values(userUpdate).every((element) => element.valid && element.touched);
      
      setIsFormValid(formValid);

  };

  const inputFields = Object.entries(userUpdate).map(([key, element]) => {
    return (
      <Form.Group className='mb-3' controlId={`ControlInput ${key}`} key={key}>
        <Form.Control
          style={element.valid ? 
            { border: 'solid rgb(15, 120, 75) 1px',
            borderRadius: '5px',
            boxShadow: '5px 5px 20px rgb(23, 88, 32)',
            marginBottom: '10px',
          }   
          : { border: "1.5px solid red",
          borderRadius: '5px',
          boxShadow: '5px 5px 20px rgb(23, 88, 32)',
          marginBottom: '10px', }}
          name={key}
          type={element.type}
          placeholder={element.placeholder}
          value={element.value}
          onChange={handleInputChange}
        />
        <div style={{fontWeight: 'bold' }}>{element.fieldReq}</div>
      </Form.Group>
    )
  });

  const cancel = () => history.push(`/users/${id}`);
  
  const updatedUser = () => {

    if (!isFormValid) {
      console.log("Form is not valid!");
      return;
    } 

    updateUser(id,
      userUpdate.firstName.value, 
      userUpdate.lastName.value, 
      userUpdate.password.value)
      .then((data) => {
        if (data.message) {
          return alert(data.message);
        }
      })
      .catch((error) => setError(error.message))
      .then(history.push(`/users/${id}`));

      if (error) {
        return (
          <h4>
            <i>An error has occurred: </i>
            {error}
          </h4>
        );
    }
  }
  
    return(
      <div className='container-edit'>
        <div
          style={{
            backgroundImage: `url(${Background})`,
            position: "fixed",
            width: "100%",
            height: "100vh",
            backgroundPosition: "center",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            top: "0",
            zIndex: "-1",
            overflowY: "scroll",
          }}
        >
          <div className='edit-user-form'>
            <Form onSubmit={updatedUser}>
              <h1 className='edit-text'>Edit profile</h1>
              <Form.Label column sm="2" style={{fontWeight: 'bold'}}>
                Email: {user.email}
              </Form.Label>
              {inputFields}
              <div className="edit-btns-group">
                <Button
                  style={{borderRadius: '20px'}}
                  type="submit"
                  variant="success"
                  id="confirm-edit"
                  disabled={!isFormValid}
                >
                  Update
                </Button>
                <Button
                  style={{borderRadius: '20px'}}
                  variant="danger"
                  id="cancel-edit"
                  onClick={cancel}
                >
                  Cancel
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    )
};

export default EditUser;
