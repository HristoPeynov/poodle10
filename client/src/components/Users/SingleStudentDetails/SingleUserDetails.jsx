import React, { useEffect, useState } from "react";
import { useContext } from "react";
import { getUserById, deleteUser } from "../../../requests/users";
import AuthContext from "../../../providers/AuthContext";
import { Button, Card, Dropdown, DropdownButton, Modal } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import { Tooltip } from "@material-ui/core";
import { BsGear } from "react-icons/bs";
import { AiOutlineDelete } from "react-icons/ai";
import './SingleUserDetails.css';
import TeacherPhoto from '../../../wallpapers/teacher-details-wallpaper.jpg'
import UsersPhoto from '../../../wallpapers/allcourses-wallpaper2.jpg'
import { STUDENT_ROLE, TEACHER_ROLE } from "../../../common/constants";


const SingleUserDetails = (props) => {

    const id = props.match.params.id;

    const { user, setUser } = useContext(AuthContext);
    
    const [userDetails, setUserDetails] = useState({});
    const [userDelete, setUserDelete] = useState(false);
    const [error, setError] = useState(null);

    const [showModal, setShowModal] = useState(false);

    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);

    const history = useHistory();

    const triggerLogout = () => {
      setUser(null);
      localStorage.removeItem("token");
      history.push("/home");
    };

    useEffect(() => {
        getUserById(id)
        .then(user => setUserDetails(user))
        .catch (error => setError(error.message));
    }, [id]);

    const deleteSingleUser = () => {
        deleteUser(id)
        .then(_ => setUserDelete(true))
        .then(triggerLogout())
        .catch (error => setError(error.message));
    }

    if (error) {
        return (
          <h4>
            <i>An error has occured: </i>
            {error}
          </h4>
        );
    }

    return (
      <div className="single-user-details">
        <div 
          style={{
            backgroundImage: `url(${UsersPhoto })`,
            position: "absolute",
            width: "100%",
            height: "100vh",
            backgroundPosition: "center",
            backgroundSize: "cover",
            backgroundRepeat: 'no-repeat',
            top: 0,
          }}
        />
        <Card style={{opacity: '.8'}}>
          <Card.Body>
            {userDetails.role === STUDENT_ROLE ? <img src="../student.png" width="100" height="100" /> 
            : <img src="../teacher.png" width="100" height="100" />}
            <div className="student-name" style={{fontFamily: 'Georgia', fontWeight: 'bold'}}>{userDetails.firstName} {userDetails.lastName}</div>
            <div style={{marginTop: '30px', fontFamily: 'Georgia'}} className="student-from">{userDetails.role} since: {new Date(userDetails.createdOn).toLocaleDateString()}</div>            
            <br />
            {userDetails.enrolledFor !== null && userDetails.enrolledFor !== 'undefined' ?
              <div>
                <DropdownButton title={`Enrolled for ${userDetails.enrolledFor?.split(',').length} courses`}>
                  <Dropdown.Item style={{fontWeight: 'bold'}}>{userDetails.enrolledFor?.split(',').map((item, i) => <div key={i}>{item}</div>)}</Dropdown.Item>
                </DropdownButton>
              </div>
           :null}
            {props.location.state && props.location.state?.teachers[0].courseOwner !== null && props.location.state?.teachers[0].courseOwner !== 'undefined' ?
              <div style={{fontSize : '25px'}}>
                <DropdownButton title={`Course Owner of ${props.location.state.teachers[0].courseOwner?.split(',').length} courses`}>
                  <Dropdown.Item style={{fontWeight: 'bold'}}>{props.location.state.teachers[0].courseOwner?.split(',').map((item, i) => <div key={i}>{item}</div>)}</Dropdown.Item>
                </DropdownButton>
              </div>
           :null}
            <br />
            {user?.sub === userDetails.id ?
              <>
                <Link to={{pathname: `/users/${id}/edit`, state: {userDetails: userDetails}}}>
                  <Tooltip title='Edit profile'>
                    <Button variant='outline-success'><BsGear /></Button>
                  </Tooltip>
                </Link>&nbsp;
                <Tooltip title='Delete profile'>
                  <Button variant='outline-success' onClick={handleShowModal}><AiOutlineDelete /></Button>
                </Tooltip>
              </>
        : null}
          </Card.Body>
        </Card>

        <Modal
          show={showModal}
          onHide={handleCloseModal}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>{`Are you sure you want to delete profile of ${userDetails.firstName} ${userDetails.lastName} ?`}</Modal.Body>
          <Modal.Footer>
            <Button
              variant='outline-success'
              onClick={() => deleteSingleUser(userDelete)}
            >
              Confirm
            </Button>
            <Button 
              variant='outline-danger'
              onClick={handleCloseModal}
            >
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
};

export default SingleUserDetails;
