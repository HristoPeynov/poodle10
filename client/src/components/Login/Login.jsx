import React, { useContext, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useHistory, Link } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import { signIn } from '../../requests/authorization';
import { getUserFromToken } from '../../utils/token';
import './Login.css'

export const Login = () => {
  const { setUser } = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const history = useHistory();

  const triggerLogin = () => {
    if (!email || !password) {
      return alert('Invalid email/password!');
    }

    signIn(email, password)
      .then((data) => {
        if (data.message) {
          return alert(data.message);
        }

        setUser(getUserFromToken(data.token));
        localStorage.setItem('token', data.token);
      })
      .then((_) => localStorage.getItem('token') && history.push('/courses'))
      .catch((error) => setError(error.message));

      if (error) {
        return (
          <h4>
            <i>An error has occurred: </i>
            {error}
          </h4>
        );
      }
  };

  return (
    <Form className='login-form'>
      <div className='welcome-text'>Learn fast, enjoy music!</div>
      <br /><br /><br />
      <Form.Group className='mb-3' controlId='ControlInput1'>
        <Form.Control
          className='login-bar'
          type='text'
          placeholder='Username'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Group>
      <Form.Group className='mb-3' controlId='ControlTextarea1'>
        <Form.Control
          className='password-bar'
          type='password'
          placeholder='******'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        &nbsp;
      </Form.Group>
      <div className='login-buttons'>
        <Button variant='outline-light' onClick={triggerLogin}>
          Login
        </Button>&nbsp;
        <Link to='/register'>
          <Button variant='outline-light'>Register</Button>
        </Link>
      </div>
    </Form>
  );
};

export default Login;
