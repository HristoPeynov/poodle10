import React, { useState, useEffect, useContext } from "react";
import { Button } from "react-bootstrap";
import AuthContext from "../../../providers/AuthContext";
import { deleteSection, getAllCourseSections, updateOrder } from "../../../requests/sections";
import IndividualSection from "../IndividualSection/IndividualSection";
import { Link, useHistory } from "react-router-dom";
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './AllSections.css'
import { getCourseById } from "../../../requests/courses";
import { TEACHER_ROLE } from "../../../common/constants";


const AllSections = (props) => {
    const [sections, setSections] = useState([])
    const [error, setError] = useState('');
    const courseId = props.match.params.courseId;
    const { user } = useContext(AuthContext);
    const [loading, setLoading] = useState(false);
    const [course, setCourse] = useState([]);
    const history = useHistory();

    useEffect(() => {
      getCourseById(courseId)
      .then(data => setCourse(data))
      .catch (error => setError(error.message));
    }, [courseId]);

    useEffect(() => {
        getAllCourseSections(courseId)
          .then((data) => setSections(data))
          .catch((error) => setError(error.message));
      }, [courseId]);

    const remove = (courseId, sectionId) => {
        deleteSection(courseId, sectionId).then((_) => {
            const resultSections = sections.filter((s) => s.id !== sectionId);
            setSections(resultSections);
        });
    };  

    const order = (updateData, courseId) => {
      setLoading(true);
      updateOrder(updateData, courseId)
      .then(r => !r.message && setSections(r.slice()))
      .finally(() => setLoading(false));
    }

    const back = () => history.push('/courses');

    if (error.message) {
        return (
          <h4>
            <i>An error has ocurred: </i>
            {error}
          </h4>
        );
    }

    const allSections = sections.length &&
    sections.map((section) => {
      return (
        <div key={section.id}>
          <IndividualSection
            {...section}
            courseId={courseId}
            remove={remove}
          />
        </div>
      );
    })
    return (
      <div className='sections-wallpaper'>
        <main className="all-sections">
          <div id='section-back-btn'>
            <Button variant='success' style={{width: '200px'}} onClick={back}>Back</Button>
          </div>
          <h3 className='course-title'>{course.title}</h3>
          {user?.role === TEACHER_ROLE && 
            <Link to={`/courses/${courseId}/sections/new`} className="create-section-link">
              <Button variant="success" style={{width: '200px'}}>
                Create new section
              </Button>
            </Link>}
          {loading && null}    
          {sections.length ? 
          user.role === TEACHER_ROLE ?
        (
          <DragDropContext onDragEnd={(params) => {
            const order1 = params.source.index;
            const order2 = params.destination?.index;
            order2 && order({order1, order2}, courseId)
          }}
          >
            <div className='list-container'>
              <Droppable droppableId="droppable-1">
                {(provided, _) => (
                  <div ref={provided.innerRef} {...provided.droppableProps}><br />
                    <ul>{sections.map((section) => (
                      <Draggable key={section.id} draggableId={section.id.toString()} index={section.orderPosition}>  
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef} {...provided.draggableProps}
                          >
                            <div {...provided.dragHandleProps}>
                              <IndividualSection
                                {...provided.dragHandleProps}
                                {...section}
                                courseId={courseId}
                                remove={remove}
                              />
                            </div>
                          </div>
                      )}
                      </Draggable>
                    ))}
                      {provided.placeholder}
                    </ul>
                  </div>
              )}
              
              </Droppable>
            </div>
          </DragDropContext>
        ) 
        : 
          <>
            <br />
            <ul>{allSections}</ul>
          </>
        
        : (
          <div id='no-sections-text'>There are no sections to show</div>
        )}
        </main>
      </div>
    )
}

export default AllSections;
