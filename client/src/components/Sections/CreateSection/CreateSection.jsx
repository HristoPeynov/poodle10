import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { createNewSection, getAllCourseSections } from "../../../requests/sections";
import { Button, Form } from "react-bootstrap";
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-html'
import 'ace-builds/src-noconflict/theme-xcode'
import 'ace-builds/src-noconflict/ext-language_tools'
import 'ace-builds/src-noconflict/ext-beautify'
import Beautify from 'ace-builds/src-noconflict/ext-beautify'
import './CreateSection.css'
import DOMPurify from "dompurify";
import sectionWallpaper from '../../../wallpapers/allcourses-wallpaper2.jpg' 

const CreateSection = (props) => {
    const history = useHistory();
    const [sections, setSections] = useState([]);
    const courseId = props.match.params.courseId;
   
    useEffect(() => {
      getAllCourseSections(courseId)
        .then((data) => { 
          setSections(data)
          const maxOrder = data.message 
          ? 0 
          : 
          data.map(item => item.orderPosition).reduce((a, b) => Math.max(a, b), 0);
          setSection({...section, orderPosition: maxOrder + 1})
        })
        .catch((error) => setError(error.message));
    }, [courseId]);

    const [section, setSection] = useState(
        { title: '',
          content: '',
          availableFrom: '',
          orderPosition: '',  
        });

    const [error, setError] = useState('');

    const create = (sectionData, id) => {
        if (!section.title || !section.content || !section.availableFrom || !section.orderPosition) {
          return alert("Please fill all input fields");
        }
        createNewSection(sectionData, id).then((r) =>
          r.message ? alert(r.message) : history.goBack()
        );
      };

    const cancel = () => history.goBack();

    const config = { ADD_TAGS: ['iframe'], KEEP_CONTENT: false }

    return (
      <div
        style={{
      backgroundImage: `url(${sectionWallpaper})`,
      position: "absolute",
      width: "100%",
      height: "100vh",
      backgroundPosition: "center",
      backgroundSize: "cover",
    }}
      >
        <Form className="create-section-form">
          <Form.Group className="cs-group" controlId="ControlInput1">
            <Form.Label style={{fontSize: '20px'}}>Title</Form.Label>
            <Form.Control
              className='create-section-input'
              type="text"
              placeholder="Title here..."
              value={section.title}
              onChange={(e) => setSection({ ...section, title: e.target.value })}
            />
          </Form.Group>
          <Form.Group className="cs-group" controlId="ControlDateInput1">
            <Form.Label style={{fontSize: '20px'}}>Set availability date:</Form.Label>
            <Form.Control
              className='create-section-input'
              type="date"
              value={section.availableFrom}
              onChange={(e) => setSection({...section, availableFrom: e.target.value})}
            />
          </Form.Group>
          <br />
          <AceEditor
            className="editor"
            style={{width: '600px', height: '200px', border: 'solid green 1px'}}
            placeholder='Content here...'
            value={section.content}
            commands={Beautify.commands}
            mode='html'
            theme='xcode'
            fontSize={16}
            showPrintMargin
            showGutter
            highlightActiveLine
            onChange={(e) => setSection({ ...section, content: DOMPurify.sanitize(e, config) })}
            setOptions={{
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true,
                    showLineNumbers: true,
                    tabSize: 4,
                    wrap: 1
                }}
          />
          <br />
          <Button variant='success' onClick={() => create(section, courseId)}>Create</Button>&nbsp;&nbsp;
          <Button variant='danger' onClick={cancel}>Cancel</Button>
        </Form>
      </div>
      );
    };


export default CreateSection;
