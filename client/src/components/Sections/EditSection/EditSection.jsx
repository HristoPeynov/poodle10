import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";
import { getSection, updateSectionEntry } from "../../../requests/sections";
import { Button, Form } from "react-bootstrap";
import "ace-builds";
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-html'
import 'ace-builds/src-noconflict/theme-xcode'
import 'ace-builds/src-noconflict/ext-language_tools'
import 'ace-builds/src-noconflict/ext-beautify'
import "ace-builds/webpack-resolver";
import Beautify from 'ace-builds/src-noconflict/ext-beautify'
import moment from 'moment';
import './EditSection.css'
import DOMPurify from "dompurify";
import editSectionWallpaper from '../../../wallpapers/allcourses-wallpaper2.jpg'

const EditSection = (props) => {
    const [error, setError] = useState("");
    const [updateSection, setUpdateSection] = useState( 
    { title: '',
    content: '',
    availableFrom: '',
    orderPosition: ''  
    });

    const history = useHistory();
    const { courseId, sectionId } = props.match.params;
    const config = { ADD_TAGS: ['iframe'], KEEP_CONTENT: false }

  //get the section and set it as an initial update state
  useEffect(() => {
    getSection(courseId, sectionId)
      .then((data) => setUpdateSection({...data, availableFrom: moment(data.availableFrom).format('YYYY-MM-DD')}))
      .catch((error) => setError(error.message));
  }, [courseId, sectionId]);
  
  //actual section update
  const update = (updateData) => {
    updateSectionEntry(courseId, sectionId, updateData).then((_) =>
      history.push(`/courses/${courseId}/sections`)
    );
  };

  const cancel = () => history.push(`/courses/${courseId}/sections`)


  return (
    <div
      style={{
      backgroundImage: `url(${editSectionWallpaper})`,
      position: "absolute",
      width: "100%",
      height: "100vh",
      backgroundPosition: "center",
      backgroundSize: "cover",
    }}
    >
      <Form className="create-section-form">
        <Form.Group className="es-group" controlId="ControlInput1">
          <Form.Label style={{fontSize: '20px'}}>Title</Form.Label>
          <Form.Control
            className='edit-section-input'
            type="text"
            placeholder="Title here..."
            value={updateSection.title}
            onChange={(e) => setUpdateSection({ ...updateSection, title: e.target.value })}
          />
        </Form.Group>
        <Form.Group className="es-group" controlId="ControlDateInput1">
          <Form.Label style={{fontSize: '20px'}}>Set availability date:</Form.Label>
          <Form.Control
            className='edit-section-input'
            type="date"
            value={moment(updateSection.availableFrom).format('YYYY-MM-DD')}
            onChange={(e) => setUpdateSection({...updateSection, availableFrom: e.target.value})}
          />
        </Form.Group>
        <br />
        <AceEditor
          className="editor"
          style={{width: '600px', height: '200px', border: 'solid green 1px'}}
          placeholder='Content here...'
          value={updateSection.content}
          commands={Beautify.commands}
          mode='html'
          theme='xcode'
          fontSize={16}
          showPrintMargin
          showGutter
          highlightActiveLine
          onChange={(e) => setUpdateSection({ ...updateSection, content: DOMPurify.sanitize(e, config) })}
          setOptions={{
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true,
                enableSnippets: true,
                showLineNumbers: true,
                tabSize: 4,
                wrap: 1
            }}
        />
        <br />
        <Button variant='success' onClick={() => update(updateSection)}>Update</Button>&nbsp;&nbsp;
        <Button variant='danger' onClick={cancel}>Cancel</Button>
      </Form>
    </div>
  );
}

export default EditSection;
