import { Tooltip } from '@material-ui/core';
import React, { useContext, useState } from 'react';
import { Button, Card, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { daysCounter } from '../../../common/helpers';
import AuthContext from '../../../providers/AuthContext';
import './IndividualSection.css';
import { BsGear } from "react-icons/bs";
import { AiOutlineDelete } from "react-icons/ai";
import { STUDENT_ROLE, TEACHER_ROLE } from '../../../common/constants';

const IndividualSection = (props) => {
    const [showModal, setShowModal] = useState(false);

    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);
    const { user } = useContext(AuthContext);
    const dateCreated = daysCounter(props.createdOn);

    return (
      <>
        <div className="individual-section">
          <Card border="success" style={{ width: "25rem" }} className="section-card">
            <Card.Img
              variant="top"
              src="https://www.picgaga.com/uploads/wallpaper/green-wallpapers/khaH2a8XbW63-free-fantastic-green-wallpaper-for-download.jpg"
              height="100"
            />
            <Card.Header>
              Created:{" "}
              {dateCreated === 0
                ? "Today"
                : dateCreated === 1
                ? `${dateCreated} day ago`
                : `${dateCreated} days ago`}
            </Card.Header>
            <Card.Body>
              <div style={{textAlign: 'center'}}>
                {user?.role === STUDENT_ROLE &&
                Date.now() < new Date(props.availableFrom).getTime() ? (
                  <>
                    <div style={{fontSize: '20px'}}>{`Title: ${props.title}`} </div>{" "}
                    <div style={{fontSize: '20px'}}>{`Section opens on ${new Date(
                      props.availableFrom
                    ).toLocaleDateString()}`}
                    </div>
                  </>
                ) : (
                  <Link style={{fontSize: '20px', color: 'teal'}} to={`/courses/${props.courseId}/sections/${props.id}`}>
                    {props.title}
                  </Link>
                )}
                {user?.role === TEACHER_ROLE && (
                  <div>
                    <div>Available from: {new Date(props.availableFrom).toLocaleDateString()}</div>
                    <Tooltip title='Edit section'>
                      <Link
                        to={`/courses/${props.courseId}/sections/${props.id}/edit`}
                      >
                        <Button variant="outline-success"><BsGear /></Button>
                      </Link>
                    </Tooltip>
                    &nbsp;&nbsp;
                    <Tooltip title='Delete section'>
                      <Button variant="outline-success" onClick={handleShowModal}><AiOutlineDelete /></Button>
                    </Tooltip>
                  </div>
                )}
              </div>
            </Card.Body>
          </Card>
        </div>

        <Modal
          show={showModal}
          onHide={handleCloseModal}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>{`Are you sure you want to delete section: ${props.title} ?`}</Modal.Body>
          <Modal.Footer>
            <Button variant='success' onClick={() => props.remove(props.courseId, props.id)}>
              Confirm
            </Button>
            <Button variant='danger' onClick={handleCloseModal}>Cancel</Button>
          </Modal.Footer>
        </Modal>
      </>
    );
}

export default IndividualSection;
