import React, { useState, useEffect, useContext} from "react";
import { deleteCourse, getAllCourses, getAllVisibleStudentCourses } from "../../../requests/courses";
import { Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import IndividualCourse from "../IndividualCourse/IndividualCourse";
import AuthContext from "../../../providers/AuthContext";
import { AiOutlineClear } from "react-icons/ai";
import './AllCourses.css'
import { Tooltip } from "@material-ui/core";
import ReactPaginate from "react-paginate";
import { coursesPerPage, TEACHER_ROLE } from "../../../common/constants";

const AllCourses = () => {
    const [courses, setCourses] = useState([]);
    const [keyWord, setKeyWord] = useState('');
    const [error, setError] = useState(null);
    const [students, setStudents] = useState([]);
    const { user } = useContext(AuthContext);
    const [pageNumber, setPageNumber] = useState(0);
    const pagesVisited = pageNumber * coursesPerPage;

    useEffect(() => {
      let isMounted = true;   
      getAllCourses()
      .then((data) => { if (isMounted) setCourses(data)})
      
      .catch((error) => setError(error.message));
      return () => { isMounted = false };
    }, []);
    
    useEffect(() => {
      let isMounted = true;  
      if(localStorage.getItem('token')) {
      getAllVisibleStudentCourses()
        .then((data) => {if (isMounted) setStudents(data)})
        .catch((error) => setError(error.message))
      }
      return () => { isMounted = false };
    }, [localStorage.getItem('token')]);  

     const remove = (courseId) => {

      deleteCourse(courseId)
      .then(_ => {
        const result = courses.filter(item => item.id !== courseId)
        setCourses(result)
      })
    };

    const changePage = ({selected}) => {
      setPageNumber(selected);
    }

    const result = () => {
      if (user) {
          if(user.role === TEACHER_ROLE) {
              return courses;
          }
         return students;
      } 
      return courses.filter(course => course.isPublic === 1);
  }

    if (error) {
        return (
          <h4>
            <i>An error has occured: </i>
            {error}
          </h4>
        );
    }
    
    const allCourses = result()
    .slice(pagesVisited, pagesVisited + coursesPerPage)
    .filter(c => keyWord === '' || (`${c.title} ${c.description}`).toLowerCase().includes(keyWord.toLowerCase()))
    .map((course) => {
      return (
        
        <div key={course.id}>

          <IndividualCourse
            {...course}
            remove={remove}
          />
      
        </div>
      );
    })

    return (
      <>
        <div className='courses-wallpaper'>
          <main className="all-courses">
            <h1 className="all-courses-text">All Courses</h1>
            {user?.role === TEACHER_ROLE &&
              <div className='create-course-btn'>
                <Link to='/courses/create'><Button variant='success'>Create course</Button></Link>
              </div>}
            <div className="search-input">
              <input type="text" className='search-input-style' value={keyWord} onChange={e => setKeyWord(e.target.value)} />
              <Tooltip title='Clear'>
                <Button variant='info' onClick={() => setKeyWord('')} id='clear-btn-style'><AiOutlineClear /></Button>
              </Tooltip>
            </div><br />
            {courses.length ? (
              <ul>{allCourses}</ul>
    ) : (
      <div>There are no courses to show</div>
    )}  
          </main>
        

          <ReactPaginate
            previousLabel="<<" nextLabel=">>" pageCount={Math.ceil(result().length/coursesPerPage)} 
            onPageChange={changePage} containerClassName="paginationButtons" previousLinkClassName="previousButton" nextLinkClassName="nextButton" disabledClassName="pagonationDisabled" activeClassName="paginationActive"
          />
        </div>
      </>
    )
}

export default AllCourses;
