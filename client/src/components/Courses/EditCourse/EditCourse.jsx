import React, { useState, useEffect } from "react";
import { getCourseById, updateCourse } from "../../../requests/courses";
import { Button, Form, ToggleButton } from "react-bootstrap";
import { useHistory } from "react-router";
import './EditCourse.css';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import editCourseWallpaper from '../../../wallpapers/allcourses-wallpaper2.jpg'

const EditCourse = (props) => {

    const courseId = props.match.params.courseId;
    const history = useHistory();
    
    const [courseUpdate, setCourseUpdate] = useState({
        title: '',
        description: '',
        isPublic: '',
    });
    const [error, setError] = useState(null);
    const [checked, setChecked] = useState(false);

    useEffect(() => {
        getCourseById(courseId)
        .then(course => setCourseUpdate(course))
        .catch (error => setError(error.message));
    }, [courseId]);

    const updatedCourse = (courseId, data) => {
        updateCourse(courseId, data)
        .then(history.goBack())
        .catch (error => setError(error.message));
    }

    const cancel = () => history.goBack();

    const IOSSwitch = withStyles((theme) => ({
      root: {
        width: 42,
        height: 26,
        padding: 0,
        margin: theme.spacing(1),
      },
      switchBase: {
        padding: 1,
        '&$checked': {
          transform: 'translateX(16px)',
          color: theme.palette.common.white,
          '& + $track': {
            backgroundColor: '#52d869',
            opacity: 1,
            border: 'solid 1 px',
          },
        },
        '&$focusVisible $thumb': {
          color: '#52d869',
          border: '6px solid #fff',
        },
      },
      thumb: {
        width: 24,
        height: 24,
      },
      track: {
        borderRadius: 26 / 2,
        border: `1px solid ${theme.palette.grey[400]}`,
        backgroundColor: theme.palette.grey[50],
        opacity: 1,
        transition: theme.transitions.create(['background-color', 'border']),
      },
      checked: {},
      focusVisible: {},
    }))(({ classes, ...props }) => {
      return (
        <Switch
          focusVisibleClassName={classes.focusVisible}
          disableRipple
          classes={{
            root: classes.root,
            switchBase: classes.switchBase,
            thumb: classes.thumb,
            track: classes.track,
            checked: classes.checked,
          }}
          {...props}
        />
      );
    });

    if (error) {
        return (
          <h4>
            <i>An error has occured: </i>
            {error}
          </h4>
        );
    }
    
        return(
          <>
            <div
              style={{
              backgroundImage: `url(${editCourseWallpaper})`,
              position: "absolute",
              width: "100%",
              height: "100vh",
              backgroundPosition: "center",
              backgroundSize: "cover",
              }}
            >
              <div id='edit-course-title-text'>{courseUpdate.title}</div>
              <div className="edit-course-form">
                <Form>
                  <Form.Group className="mb-3" controlId="ControlInput2">
                    <Form.Label style={{fontWeight: 'bold'}}>Title</Form.Label>
                    <Form.Control
                      style={{
                    border: 'solid rgb(15, 120, 75) 1px',
                    width: '500px',
                    borderRadius: '5px',
                    boxShadow: '5px 5px 20px rgb(23, 88, 32)',
                    marginBottom: '10px',
                  }}
                      type="text"
                      placeholder="Title"
                      value={courseUpdate.title}
                      onChange={(e) => {courseUpdate.title = e.target.value; setCourseUpdate({...courseUpdate})}}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="ControlInput2">
                    <Form.Label style={{fontWeight: 'bold'}}>Description</Form.Label>
                    <Form.Control
                      style={{
                    border: 'solid rgb(15, 120, 75) 1px',
                    borderRadius: '5px',
                    boxShadow: '5px 5px 20px rgb(23, 88, 32)',
                    marginBottom: '10px',
                    minHeight: '200px',
                  }}
                      as="textarea"
                      placeholder="Description"
                      value={courseUpdate.description}
                      onChange={(e) => {courseUpdate.description = e.target.value; setCourseUpdate({...courseUpdate})}}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <FormControlLabel
                      control={<IOSSwitch
                        checked={Boolean(courseUpdate.isPublic)} 
                        onChange={(e) => {setChecked(e.currentTarget.checked); courseUpdate.isPublic = e.currentTarget.checked; setCourseUpdate({...courseUpdate})}}
                               />}
                      label="Set to public"
                    />
                  </Form.Group>
                  <div className='edit-course-btns'>
                    <Button className="button" variant='success' style={{marginRight: '5px', width: '70px'}} onClick={() => updatedCourse(courseId, courseUpdate)}>Edit</Button>
                    <Button className="button" variant='danger' style={{marginLeft: '5px'}} onClick={cancel}>Cancel</Button>   
                  </div>      
                </Form>
              </div>
            </div>
          </>
    )
};

export default EditCourse;


