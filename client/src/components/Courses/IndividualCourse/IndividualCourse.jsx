import { Tooltip } from "@material-ui/core";
import React, { useContext, useState } from "react";
import { useEffect } from "react";
import { Accordion } from "react-bootstrap";
import { Button, Card, Modal } from "react-bootstrap";
import { AiOutlineDelete } from "react-icons/ai";
import { BsGear } from "react-icons/bs";
import { FiUsers } from "react-icons/fi";
import { Link, useHistory } from 'react-router-dom';
import { STUDENT_ROLE, TEACHER_ROLE } from "../../../common/constants";
import { daysCounter } from "../../../common/helpers";
import AuthContext from "../../../providers/AuthContext";
import { getEnrolledStudents, getCourseById, deleteCourse } from "../../../requests/courses";
import { selfEnroll, selfUnenroll } from "../../../requests/users";
import './IndividualCourse.css'

const IndividualCourse = (props) => {

    const courseId  = props.id;
    
    const [enrolled, setEnrolled] = useState([]);
    const { user, setUser } = useContext(AuthContext);
    const history = useHistory();
    
    const [courses, setCourses] = useState([]);
    const [error, setError] = useState(null);

    const dateCreated = daysCounter(props.createdOn);

    const [showModal, setShowModal] = useState(false);

    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);

    const [showModalEnroll, setShowModalEnroll] = useState(false);
    const [showModalUnenroll, setShowModalUnenroll] = useState(false);

    const handleCloseModalEnroll = () => setShowModalEnroll(false);
    const handleShowModalEnroll = () => setShowModalEnroll(true);

    const handleCloseModalUnenroll = () => setShowModalUnenroll(false);
    const handleShowModalUnenroll = () => setShowModalUnenroll(true);
    useEffect(() => {
      if (localStorage.getItem('token')) {
        getCourseById(courseId)
        .then(data => setCourses(data))
        .catch (error => setError(error.message));
      }
    }, [courseId, localStorage.getItem('token')]);


    useEffect(() => {
      if (localStorage.getItem('token')) {
        getEnrolledStudents(props.id)
          .then((data) =>setEnrolled(data))
      }
      }, [localStorage.getItem('token')]);

      const enroll = (courseId) => {
        selfEnroll(courseId).then((_) =>
          history.push(`/courses/${courseId}/sections`)
        );
      };

      const unenroll = (courseId) => {
        selfUnenroll(courseId).then(_ => {
          const result = enrolled.filter(item => item.email !== user.email)
          setEnrolled(result);
        })
      }

      const enrolledEmail = enrolled.map(item => item.email);

    return (
      <>
        {(props.isPublic === 1 || user?.role === TEACHER_ROLE || enrolledEmail.includes(user?.email)) &&       
          <div className='individual-course'>
            <Card style={{width: '90%'}} className='course-card'>
              <Card.Header>
                Created:{" "}
                {dateCreated === 0
                ? "Today "
                : dateCreated === 1
                ? `${dateCreated} day ago `
                : `${dateCreated} days ago `}
                {user?.role === TEACHER_ROLE 
                ? !props.isPublic 
                ? <span style={{color: 'red'}}>Private</span> 
                : <span style={{color: 'green'}}>Public</span> 
                : null}
                {user?.role === TEACHER_ROLE &&
                  <span className='teacher-course-menu-btns'>
                    {!props.isPublic && 
                      <Link to={`courses/${props.id}/enroll`}>
                        <Tooltip title='Manage students'>
                          <Button variant='outline-success' style={{width: '55px'}}><FiUsers /></Button>
                        </Tooltip>
                      </Link>}&nbsp;
                    <Link to={`courses/${props.id}/edit`}>
                      <Tooltip title='Edit course'>
                        <Button variant='outline-success' style={{width: '55px'}}><BsGear /></Button>
                      </Tooltip>
                    </Link>&nbsp;
                    <Tooltip title='Delete course'>
                      <Button variant='outline-success' style={{width: '55px'}} onClick={handleShowModal}><AiOutlineDelete /></Button>
                    </Tooltip>
                  </span>}
                {user?.role === STUDENT_ROLE ? 
                enrolledEmail.includes(user.email) ?
                  <Tooltip title='Unenroll from course'>
                    <Button variant='outline-danger' className='student-unenroll-btn' onClick={handleShowModalUnenroll}>Unenroll</Button>
                  </Tooltip>
              : <Tooltip title='Enroll to course'><Button className='student-enroll-btn' variant='outline-success' onClick={handleShowModalEnroll}>Enroll</Button></Tooltip>
              : null}
              </Card.Header>
              <Card.Body>
                <Card.Title>
                  <h4 className='card-title-text'>{user?.role === TEACHER_ROLE ?
                    <Link className='title-link' to={`/courses/${props.id}/sections`}>{props.title}</Link> 
              : user?.role === STUDENT_ROLE ? 
                enrolledEmail.includes(user.email) ?
                  <Link className='title-link' to={`/courses/${props.id}/sections`}>{props.title}</Link>
                  : <>{props.title}</>
                  : props.title}
                    
                  </h4>
                </Card.Title>
                <Accordion>
                  <Card>
                    <Accordion.Toggle className='course-description-toggle' eventKey={props.id}>
                      View description
                    </Accordion.Toggle>

                    <Accordion.Collapse eventKey={props.id}>
                      <Card.Body><div style={{textAlign: 'justify'}}>{user?.role === STUDENT_ROLE ? courses.description : props.description}</div></Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
              </Card.Body>
            </Card>
          </div>}

        <Modal
          show={showModalEnroll}
          onHide={handleCloseModalEnroll}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>{`Are you sure you want to enroll for course: ${props.title} ?`}</Modal.Body>
          <Modal.Footer>
            <Button
              variant='outline-success'
              onClick={() => enroll(props.id)}
            >
              Confirm
            </Button>
            <Button 
              variant='outline-danger'
              onClick={handleCloseModalEnroll}
            >
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal
          show={showModalUnenroll}
          onHide={handleCloseModalUnenroll}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>{`Are you sure you want to unenroll from course: ${props.title} ?`}</Modal.Body>
          <Modal.Footer>
            <Button
              variant='outline-success'
              onClick={() => {unenroll(props.id); handleCloseModalUnenroll()}}
            >
              Confirm
            </Button>
            <Button
              variant='outline-danger' 
              onClick={handleCloseModalUnenroll}
            >
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal
          show={showModal}
          onHide={handleCloseModal}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Body>{`Are you sure you want to delete course: ${props.title} ?`}</Modal.Body>
          <Modal.Footer>
            <Button
              variant='outline-success'
              onClick={() => props.remove(courseId)}
            >
              Confirm
            </Button>
            <Button 
              variant='outline-danger'
              onClick={handleCloseModal}
            >
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </>  
    )
}

export default IndividualCourse;
