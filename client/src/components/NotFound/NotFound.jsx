import React from 'react';

const NotFound = () => {
  return (
    <div className="NotFound" style={{marginTop: '100px', textAlign: 'center'}}>
      <h1>The page was not found!</h1>
    </div>
  );
};

export default NotFound;
