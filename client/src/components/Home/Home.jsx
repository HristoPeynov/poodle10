import React, { useContext } from 'react';
import AuthContext from '../../providers/AuthContext';
import AllCourses from '../Courses/AllCourses/AllCourses';
import Login from '../Login/Login';
import PowerNotes from './video/Noten1.mp4'

const Home = () => {

  const { user, setUser } = useContext(AuthContext);

  if (!user) {
    return (
      <div className='home-page'>
        <video
          autoPlay loop muted
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            transform: 'translate(-50% -50%)',
            zIndex: '-1'
        }}
        >
          <source src={PowerNotes} type='video/mp4' />
        </video>
        <Login />
      </div>
)
} return (<AllCourses />)}

export default Home;
