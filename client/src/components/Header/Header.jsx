import React, { useContext } from "react";
import { NavLink, useHistory, withRouter } from "react-router-dom";
import { Nav, Navbar, Container, Button } from "react-bootstrap";
import AuthContext from "../../providers/AuthContext";
import { Avatar, makeStyles, Tooltip } from "@material-ui/core";
import './Header.css'
import { AiOutlineHome } from "react-icons/ai";
import { BsBook, BsInfoSquare } from "react-icons/bs";
import { FiUsers } from "react-icons/fi";

const Header = (props) => {
  const { location } = props;
  const history = useHistory();
  const { user, setUser } = useContext(AuthContext);

  const triggerLogout = () => {
    setUser(null);
    localStorage.removeItem("token");
    history.push("/home");
  };

  const useStyles = makeStyles({
    avatar: {
      background: 'radial-gradient(circle, rgba(54,198,228,1) 6%, rgba(15,176,235,0.9812967423297444) 100%)',
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    },
  });

  const classes = useStyles()

  return (
    <Navbar bg="success" variant="dark">
      <Container>
        <Navbar.Brand as={NavLink} exact to="/home" className="logo-text">
          <div className='logo'>
            MYsic compoSITE
          </div>
        </Navbar.Brand>
        <Nav activeKey={location.pathname} className="ml-auto">
          <Tooltip title='Home'>
            <Nav.Link as={NavLink} exact to="/home">
              <AiOutlineHome style={{fontSize: '25px'}} />
            </Nav.Link>
          </Tooltip>
          <Tooltip title='Courses'>
            <Nav.Link as={NavLink} exact to="/courses">
              <BsBook style={{fontSize: '25px'}} />
            </Nav.Link>
          </Tooltip>
          <Tooltip title='Students'>
            <Nav.Link as={NavLink} exact to="/users">
              <FiUsers style={{fontSize: '25px'}} />
            </Nav.Link>
          </Tooltip>
          <Tooltip title='About'>
            <Nav.Link as={NavLink} exact to="/about">
              <BsInfoSquare style={{fontSize: '25px'}} />
            </Nav.Link>
          </Tooltip>
          {user && (
            <>
              <Button variant='outline-light' className='logout-btn' onClick={triggerLogout}>
                Logout
              </Button>
              &nbsp;&nbsp;
            </>
          )}
          {user?
            <Tooltip title='My Profile'>
              <Avatar className={classes.avatar}>
                <Nav.Link as={NavLink} exact to={`/users/${user.sub}`}>
                  {`${(user.firstName).toUpperCase().slice(0, 1)}${(user.lastName).toUpperCase().slice(0, 1)}`}
                </Nav.Link>
              </Avatar>
            </Tooltip>
        : null}
        &nbsp;&nbsp;
        </Nav>
      </Container>
    </Navbar>
  );
};

export default withRouter(Header);
