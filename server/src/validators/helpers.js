import sanitizeHTML from 'sanitize-html';

export const validId = (values = []) => {

  const isNumber = /^[0-9]+$/;
  return values.every(item => isNumber.test(item)); 
};

export const cleanHTML = (data) => sanitizeHTML(data, {
  allowedTags: sanitizeHTML.defaults.allowedTags.concat([ 'iframe' ]),
  allowedAttributes: {
      'iframe': ['src', 'height', 'width', 'allowfullscreen'],
    },
    allowedIframeHostnames: ['www.youtube.com', 'player.vimeo.com'],
});
