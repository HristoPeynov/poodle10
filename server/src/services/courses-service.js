import errors from './errors.js';

const getAllCourses = (coursesData) => {
  
  return coursesData.getAll();

};

const getCourseById = coursesData => {
  return async (id) => {
      const course = await coursesData.getBy('c.id', id);

      if (!course) {
          return {
              error: errors.RECORD_NOT_FOUND,
              course: null,
          };
      }

      return { error: null, course: course };
  };
};

const createCourse = coursesData => {
  return async (coursesCreate) => {
      const { title, description, userId } = coursesCreate;

      const existingCourse = await coursesData.getBy('c.title', title);

      if (existingCourse) {
          return {
              error: errors.DUPLICATE_RECORD,
              course: null,
          };
      }

      const course = await coursesData.create(title, description, userId);

      return { error: null, course: course };
  };
};

const updateCourse = coursesData => {
  return async (id, courseUpdate) => {
      const course = await coursesData.getBy('c.id', id);

      if (!course) {
          return {
              error: errors.RECORD_NOT_FOUND,
              course: null,
          };
      }

      const updated = { ...course, ...courseUpdate };
      const _ = await coursesData.update(updated);

      return { error: null, course: updated };
  };
};

const deleteCourse = coursesData => {
  return async (id) => {
      const courseToDelete = await coursesData.getBy('c.id', id);
      if (!courseToDelete) {
          return {
              error: errors.RECORD_NOT_FOUND,
              course: null,
          };
      }

      const _ = await coursesData.remove(id);
      
      return { error: null, course: courseToDelete };
  };
};

const enrollStudent = coursesData => {
  return async (courseId, userId) => {
      const courseToEnroll = await coursesData.getBy('c.id', courseId);

      if(!courseToEnroll) {
          return {
              error: errors.RECORD_NOT_FOUND,
              enroll: null,
          };
      }

          /*eslint no-unused-vars: 'off'*/
          try {
              const _ = await coursesData.enroll(courseId, userId);
          } catch (e) {
              return {
                  error: errors.DUPLICATE_RECORD,
                  enroll: null,
              };
          }
          

          const allUnenrolled = await coursesData.getAllUnenrolledStudents(courseId);
          const allEnrolled = await coursesData.getEnrolledStudents(courseId);

          return { error: null, enroll: {...courseToEnroll, allUnenrolled, allEnrolled }};
  };
};

const getAllEnrolledByCourse = coursesData => {
    return async (courseId) => {
        const existingCourse = await coursesData.getBy('c.id', courseId);

        if(!existingCourse) {
            return {
                error: errors.RECORD_NOT_FOUND,
                enroll: null,
            };
        }

        const allEnrolled = await coursesData.getEnrolledStudents(courseId);

        if(!allEnrolled) {
            return {
                error: errors.RECORD_NOT_FOUND,
                enroll: null,
            };
        }

        return {
            error: null,
            enroll: allEnrolled,
        };
    };
};

const unenrollStudent = coursesData => {
    return async (courseId, userId) => {
        const existingCourse = await coursesData.getBy('c.id', courseId);
  
        if(!existingCourse) {
            return {
                error: errors.RECORD_NOT_FOUND,
                enroll: null,
            };
        }
            
            const _ = await coursesData.unenroll(courseId, userId);
            const result = await coursesData.getEnrolledStudents(courseId);
  
            return { error: null, enroll: result};
    };
};

const unenrollStudentByTeacher = coursesData => {
    return async (courseId, userId) => {
        const existingCourse = await coursesData.getBy('c.id', courseId);
  
        if(!existingCourse) {
            return {
                error: errors.RECORD_NOT_FOUND,
                enroll: null,
            };
        }
            
            const _ = await coursesData.unenroll(courseId, userId);
            const allEnrolled = await coursesData.getEnrolledStudents(courseId);
            const allUnenrolled = await coursesData.getAllUnenrolledStudents(courseId);
  
            return { error: null, enroll: {enrolled: allEnrolled, unenrolled: allUnenrolled} };
    };
};

const unenrolledByCourse = coursesData => {
    return async (courseId) => {
  
      const currentCourse = await coursesData.getBy('c.id', courseId);
      if (!currentCourse) {
        return {
          error: errors.RECORD_NOT_FOUND,
          user: null,
        };
      }
  
      const result = await coursesData.getAllUnenrolledStudents(courseId);
      return { error: null, user: result};
    }; 
  };

  const getAllVisibleCourses = (coursesData) => {
    return async (id) => {
 
    const allVisible = await coursesData.getAllVisibleCoursesByStudent(id);
 
    return {allVisible: allVisible};
  };
 };

export default {
  getAllCourses,
  getCourseById,
  createCourse,
  updateCourse,
  deleteCourse,
  enrollStudent,
  getAllEnrolledByCourse,
  unenrollStudent,
  unenrolledByCourse,
  unenrollStudentByTeacher,
  getAllVisibleCourses,
};
