import pool from './pool.js';

const getAllStudents = async () => {
  const sql = `
    SELECT u.id, u.firstName, u.lastName, u.email, u.createdOn, group_concat(c.title) as enrolledFor
    FROM users u
    LEFT JOIN enrolled e
    ON u.id = e.userId
    LEFT JOIN courses c on c.id = e.courseId
    WHERE u.roleId = 2
    AND u.isDeleted = 0
    group by u.id
    order by u.firstName ASC;
  `;

  return await pool.query(sql);
};

const getAllTeachers = async () => {
  const sql = `
  SELECT u.id, u.firstName, u.lastName, u.email, u.createdOn, group_concat(c.title) as courseOwner
  FROM users u
  LEFT JOIN courses c
  ON u.id = c.userId
  WHERE u.roleId = 1
  AND u.isDeleted = 0
  group by u.id
  order by u.firstName ASC;
  `;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
    SELECT u.id, u.firstName, u.lastName, u.email, u.createdOn, r.name as role, group_concat(c.title) as enrolledFor
    FROM users u
    LEFT JOIN enrolled e
    ON u.id = e.userId
    LEFT JOIN courses c on c.id = e.courseId
    LEFT JOIN roles r ON u.roleId = r.id
    WHERE ${column} = ?
    AND u.isDeleted = 0
    group by u.id
    order by u.firstName ASC
    `;

  const result = await pool.query(sql, [value]);

  return result[0];
};

const getWithRole = async (email) => {
  const sql = 
  `SELECT u.id, u.email, u.firstName, u.lastName, u.password, r.name as role
   FROM users u
   JOIN roles r ON u.roleId = r.id
   WHERE u.email = ?
   AND u.isDeleted = 0`;

  const result = await pool.query(sql, [email]);

  return result[0];

};

const searchBy = async (column, value) => {
  const sql = `
      SELECT id, email, firstName, lastName 
      FROM users
      WHERE isDeleted = 0
      AND ${column} LIKE '%${value}%' 
  `;

  return await pool.query(sql);
};

const create = async (email, firstName, lastName, password, role) => {
  const sql = `
   INSERT INTO users(email, firstName, lastName, password, roleId)
   VALUES (?,?,?,?,(SELECT id FROM roles WHERE name = ?))`;

  const result = await pool.query(sql, [email, firstName, lastName, password, role]);

  return {
    id: result.insertId,
    email: email,
  };

};

const update = async (user) => {
  const { id, firstName, lastName, password } = user;
  const sql = `
      UPDATE users 
      SET firstName = ?, lastName = ?, password = ?
      WHERE isDeleted = 0
      AND id = ?
  `;

  return await pool.query(sql, [firstName, lastName, password, id]);
};

const remove = async (user) => {
  const sql = `
      UPDATE users
      SET isDeleted = 1 
      WHERE id = ?
  `;

  return await pool.query(sql, [user.id]);
};

export default {
  getAllStudents,
  getAllTeachers,
  getBy,
  getWithRole,
  searchBy,
  create,
  update,
  remove,
};
