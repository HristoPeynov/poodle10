import express from 'express';
import { STUDENT_ROLE, TEACHER_ROLE } from '../../config.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import sectionsData from '../data/sections-data.js';
import sectionsService from '../services/sections-service.js';
import errors from '../services/errors.js';
import bodyValidator from '../validators/validate-body.js';
import createSectionSchema from '../validators/create-section-validator.js';
import { validId } from '../validators/helpers.js';
import { cleanHTML } from '../validators/helpers.js';


const sectionsController = express.Router();

sectionsController

  // get all sections
  .get('/:id/sections', async (req, res) => {
        authMiddleware,
        roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]);

        const { id } = req.params;

        if(!validId([id])) return res.status(404).send({ message: 'Section not found' });
        const { error, section } = await sectionsService.getAllSections(sectionsData)(+id);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Section not found!' });
       } else {
            res.status(200).send(section);
       }
    })

    // get section by Id

   .get('/:id/sections/:sectionId',
        async (req, res) => {
            const { id, sectionId } = req.params;

            if(!validId([id, sectionId])) return res.status(404).send({ message: 'Course not found' });

            const { error, section } = await sectionsService.getSectionById(sectionsData)(+id, +sectionId);

            if (error === errors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Course not found!' });
            } else {
                res.status(201).send(section);
            }
        },
   ) 

   // create section
   .post('/:id/sections',
        authMiddleware,
        roleMiddleware([TEACHER_ROLE]),
        bodyValidator(createSectionSchema),
        async (req, res) => {

            const createSection = req.body;
            createSection.content = cleanHTML(req.body.content);
            createSection.courseId = +req.params.id;
            
            const { error, section } = await sectionsService.createSection(sectionsData)(createSection);

            if (error === errors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Course not found!' });
            } else {
                res.status(201).send(section);
            }
        },
    )
    
    // update section by id
    .put('/:id/sections/:sectionId',
        authMiddleware,
        roleMiddleware([TEACHER_ROLE]),
        async (req, res) => {
            const updatedSection = req.body;
            const { id, sectionId } = req.params;

            updatedSection.content = cleanHTML(req.body.content);

            const { error, section } = await sectionsService.updateSection(sectionsData)(+sectionId, updatedSection, +id);

            if (error === errors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Course or section not found!' });
            } else {
                res.status(200).send(section);
            }
        },
    )

      // delete section by id
    .delete('/:id/sections/:sectionId',
        authMiddleware,
        roleMiddleware([TEACHER_ROLE]),
        async (req, res) => {
            const { id, sectionId } = req.params;

            const { error, section } = await sectionsService.deleteSection(sectionsData)(+id, +sectionId);

            if (error === errors.RECORD_NOT_FOUND) {
                return res.status(404).json({ message: 'Course or section not found!' });
            } 
            res.status(200).json(section);
        },
    )

    .post('/:courseId/sections/order',
    authMiddleware,
    roleMiddleware([TEACHER_ROLE]),
    async (req, res) => {
        const updatedOrder = req.body;
        const courseId = +req.params.courseId;

        const { error, section } = await sectionsService.updateOrder(sectionsData)(updatedOrder, courseId);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Course or section not found!' });
        } else {
            res.status(200).send(section);
        }
    },
);    

export default sectionsController;