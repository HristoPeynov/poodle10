import express from 'express';
import usersData from '../data/users-data.js';
import usersService from '../services/users-service.js';
import errors from '../services/errors.js';
import createUserSchema from '../validators/create-user-validator.js';
import updateUserSchema from '../validators/update-user-validator.js';
import bodyValidator from '../validators/validate-body.js';
import { STUDENT_ROLE, TEACHER_ROLE } from '../../config.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import bcrypt from 'bcrypt';

const usersController = express.Router();

usersController

  // get all Students with searching
  .get('/', async (req, res) => {
    const { search } = req.query;
    const users = await usersService.getStudents(usersData)(search);

    res.status(200).send(users);
  })

  // get all Teachers with searching
  .get('/teachers', async (req, res) => {
    const { search } = req.query;
    const users = await usersService.getTeachers(usersData)(search);

    res.status(200).send(users);
  })

  // get user by id
  .get('/:id',
  authMiddleware,
  async (req, res) => {
      const { id } = req.params;
      const { error, user } = await usersService.getUserById(usersData)(+id);

      if (error === errors.RECORD_NOT_FOUND) {
          res.status(404).send({ message: 'User not found!' });
      } else {
          res.status(200).send(user);
      }
  })

  // create user
  .post('/', bodyValidator(createUserSchema), async (req, res) => {
    const createData = req.body;

    const { error, user } = await usersService.createUser(usersData)(
      createData,
    );
    if (error === errors.DUPLICATE_RECORD) {
      res.status(409).send({ message: 'Email not available' });
    } else {
      res.status(201).send(user);
    }
  })

  // update user by id
  .put('/:id',
  authMiddleware,
  roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]),
  bodyValidator(updateUserSchema),
  async (req, res) => {
      const id  = +req.params.id;
      const updateData = req.body;

      const passwordHash = await bcrypt.hash(updateData.password, 10);
      updateData.password = passwordHash;

      const { error, user } = await usersService.updateUser(usersData)(id, updateData);

      if (error === errors.RECORD_NOT_FOUND) {
          res.status(404).send({ message: 'User not found!' });
      } else if (id !== req.user.id) {
          res.status(400).send({ message: 'You are not allowed to update other users credentials!' });
      } else {
          res.status(200).send(user);
      }
  })

  // delete user by id
  .delete('/:id', 
  authMiddleware,
  roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]),
  async (req, res) => {
  const id = +req.params.id;
  const { error, user } = await usersService.deleteUser(usersData)(id);

  if (error === errors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'User not found!' });
  } else {
      res.status(200).send({ message: `User ${user.username} is deleted` });
  }
});

export default usersController;
